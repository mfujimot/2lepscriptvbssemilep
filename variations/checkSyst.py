import ROOT as r
import numpy as np
from array import array
#from optparse import OptionParser
import argparse
from ROOT import TH2F,TKDE, TCanvas,TFile, TCanvas, TStyle, TAxis, gStyle, TColor, TPad, TH1F, kBlue,kRed,kSpring,kBlack,kGray,kCyan,kOrange, kViolet, kGreen, gROOT, TLatex, TLine, TF1,TH1D, TGraphErrors, TAxis, kFullCircle, kFALSE,kTRUE, TLegend,kAzure, TGraph
from math import fabs
import glob
import math
import os

gROOT.SetBatch(kTRUE);

# Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--dir', help = 'path to root file with pdf uncertainties')
parser.add_argument('--reg', type= int, help = '0 is resolved, 1 is boosted' )
args = parser.parse_args()


# Some functions
def getNormhist(hist):
    h_temp= hist.Clone()
    norm= 1/h_temp.Integral(0,h_temp.GetNbinsX()+1) #TH1::Integral returns the integral of bins in the bin range (default(1,Nbins), to include the Under/Overflow, use h.Integral(0,Nbins+1)
    h_temp.Scale(norm)

    return h_temp

def getDataMCRatio(h_data,h_MC):
    h_comp= h_data.Clone()
    h_comp.Reset()

    for bin_x in range(0,h_comp.GetNbinsX()+1):
        nData = h_data.GetBinContent(bin_x);
        eData = h_data.GetBinError(bin_x);
        nMC = h_MC.GetBinContent(bin_x);
        if nMC > 0:
            nComp = (nData) / nMC;
            eComp = eData / nMC;
            h_comp.SetBinContent(bin_x, nComp);
            h_comp.SetBinError(bin_x, eComp);
            #print(h_MC.GetBinCenter(bin_x),nComp)
            pass

        pass
    pass
    return h_comp;


#uncertainty sets
sys_names = [
#"MJJREWEIGHT_Sherpa221",
#"MJJREWEIGHT_100per",
#"JET_Pileup_OffsetMu",
#"JET_Pileup_PtTerm",
#"JET_Pileup_OffsetNPV",
#"JET_Pileup_RhoTopology",
#"PRW_DATASF"
#"TheoryPDF_Z",
#"TheoryPDF_VBS",
#"TheoryPDF_NNPDF_Z",
#"TheoryPDF_NNPDF_VBS",
#"TheoryQCD_W",
#"EWQCDint"
"MODEL_sig_Hw"
#"TheoryQCD_VBS",
#
#"MODEL_VV_PwPy", #minori new doprutere
#"MODEL_VV_PwPy",
#"MODEL_Z_MGPy8",
#
#"JET_Flavor_Composition",
#"JET_Flavor_Response",
#"JET_JvtEfficiency",
#"QG_exp",
#"QG_me",
#"QG_pdf",
#"QG_trackeff",
#"QG_fake"
#
#"FATJET_JER"
]
#uncertainty sets for BTags
sys_names_btag = [
"FT_EFF_Eigen_B_0_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_B_1_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_B_2_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_0_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_1_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_2_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_3_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_0_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_1_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_2_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_3_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_4_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_extrapolation_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_extrapolation_from_charm_AntiKt4EMPFlowJets_BTagging201903"
]


#--------------------------
# User configuration
#--------------------------------------------------------
filename= str(args.dir) 
f=r.TFile(filename,"READ")
f_sys = f.GetDirectory("Systematics/");
print("Moved********")

#minori new sample
#filenameNew="/sps/atlas/m/mfujimot/mergedsamples/Histograms.VBS.2lep_local.13TeV.mc16ade.24jan22_BTaggingSyst.root" 
#filenameNew="/sps/atlas/m/mfujimot/mergedsamples/Histograms.VBS.2lep_local.13TeV.mc16ade.31jan22_SystJunk.root"
#filenameNew="/sps/atlas/m/mfujimot/mergedsamples/Histograms.VBS.2lep_local.13TeV.mc16ade.09fev22_QGSystOnly.root";
#filenameNew="/sps/atlas/m/mfujimot/mergedsamples/2202samplesfJvt/Histograms.VBS.2lep_local.13TeV.mc16ade.22fev22_QGSystOnly.root";
#if int(args.reg) == 0:
#	fNew=r.TFile(filenameNew,"RECREATE")
#elif int(args.reg) == 1:
#	fNew=r.TFile(filenameNew,"UPDATE")
#fNew_sys = fNew.mkdir("Systematics/");

# sample and associated pdf uncertainties array
#samples = ["ZZ","WZ","WW","EW6llqq","Z","W","ttbar","stops","stopt","stopWtDilep"] 
samples = ["EW6llqq"] 
uncertainty_sets = [sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names] # each sample comes with its  corresponding uncertainty arrays_names_btag
#uncertainty_sets = [sys_names_btag,sys_names_btag,sys_names_btag,sys_names_btag,sys_names_btag,sys_names_btag,sys_names_btag,sys_names_btag,sys_names_btag,sys_names_btag] # each sample comes with its  corresponding uncertainty arrays_names_btag

if int(args.reg) == 0:
        #Resolved
        regions= ["SRVBS_Fid","CRVjet_Fid"]
        prefix = ["0ptag2pjet_0ptv","0ptag2pjet_0ptv","0ptag2pjet_0ptv"] #for each region you need to add the corresponding prefix 
        variables = ["RNNScoreResolved","MTagResJets"]#,
elif int(args.reg) == 1:
        # Merged 
        regions= ["SRVBS_HP","SRVBS_LP","CRVjet"] 
        prefix = ["0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv"]
        variables = ["RNNScoreMerged","MTagMerJets"]#,
else:
        print("Invalid regime to scan")

#should be consistent with variables
if int(args.reg) == 0:
	x_min = [0.,400,0]#,0,0,0,-5,-5,0,0,0,0,0,0,-5,0,0,0,0]
	x_max = [1.,4000,3000]#,10,1000,1000,5,5,60,60,0.4,0.4,1000,1000,5,60,60,0.4,0.4]
	rebin = [10,10,10]#,1,5,5,2,2,2,2,1,1,5,5,2,2,2,1,1] 
        #                ptpte e NtNtWiWiPtPte NtNtWiWi
elif int(args.reg) == 1:
	x_min = [0.,400,0]#,0,0,0,-5,-5,0,0,0,0]
	x_max = [1.,4000,3000]#,10,1000,1000,5,5,60,60,0.4,0.4]
	rebin = [10,10,10]#,1,5,5,2,2,2,2,1,1] 
        #                PtPte e NtNtWiWi

doRebin = True
doNorm = False
doConPlots = True

sample_iter=0
for sample in samples: #samples loop (Z)
    var_iter = 0
    for variable in variables: #var loop (RNNSCore)
        region_iter=0
        for region in regions: #region loop (SRVBS)
            # Get Nominal hist
            keyName=sample+"_"+prefix[region_iter]+"_"+region+"_"+variable
            print("Get Nominal hist for: ",keyName)
            f.cd()
            hist_nom = f.Get(keyName)
            #print("nbins= ",hist_nom.GetNbinsX())
            if "TObject" in str(type(hist_nom)) :
                print(keyName, " hist doesn't exist")
                continue

            #norm hist should be rebinned only once
            if doRebin:
                hist_nom.Rebin(rebin[var_iter])
            
            #for up syst
            h_new = hist_nom.Clone()
            h_new.Reset()

            #for down syst
            #h_new_d = hist_nom.Clone()
            #h_new_d.Reset()
 
            #mv into sys directory
            f_sys.cd()
            print("Moved 2********")

            #print norm integral
            print(hist_nom.Integral(), "nominal integral")

            # loop over variations
            for j in range(len(uncertainty_sets[sample_iter])): #syst loop
                uncertainty_set = uncertainty_sets[sample_iter]
                variation = variable+"_Sys"+str(uncertainty_set[j])
                keyName=sample+"_"+prefix[region_iter]+"_"+region+"_"+variation
                #print("Get variation: ",keyName)
                h_new = f_sys.Get(keyName+"__1down") 
                #h_new_d = f_sys.Get(keyName+"__1down") 
                if "TObject" in str(type(h_new)) :
                    print(keyName+"__1down", " hist doesn't exist")
	    	    continue
                print(h_new.Integral(), "down integral") #minori
                #if "TObject" in str(type(h_new_d)) :
                #    print(keyName+"__1down", " hist doesn't exist")
	    	#    continue
                #print(h_new_d.Integral(), "down integral") #minori

                # write new combined variation hist to output file
                #f_sys.cd()
                #print("Moved********")
                print("get out keyname")
                out_keyName=keyName+"__1down"
                #out_keyName_d=keyName+"__1down"
                print("Writing: ", out_keyName)
                #print("Writing: ", out_keyName_d)
                #minori
                #fNew_sys.WriteObject(h_new,out_keyName)
                #fNew_sys.WriteObject(h_new_d,out_keyName_d)
                    
                print("before printing plots")
                if doConPlots:
                    print("printing plots")
                    pdfdir = "./ControlPlots/"
                    if not os.path.exists(pdfdir) :
                        os.makedirs(pdfdir)
        
                    canv=TCanvas("","",60,60,600,600)
                    P_1 = TPad("Hists","", 0, 0.35, 0.94, 1);
                    P_2 =TPad("Data/Bgd","", 0, 0.1, 0.94, 0.35); #xmin,ymin,xmax,ymax

                    P_1.Draw();
                    P_2.Draw();

                    P_1.SetBottomMargin(0.02);
                    P_1.SetTopMargin(0.07);
                    P_1.SetRightMargin(0.15);
                    P_1.SetLeftMargin(0.15);
    
                    P_2.SetTopMargin(0.07);
                    P_2.SetBottomMargin(0.3);
                    P_2.SetRightMargin(0.15);
                    P_2.SetLeftMargin(0.15);
    
                    P_1.cd()
        
                    if doRebin:
                        h_new.Rebin(rebin[var_iter])
                        #h_new_d.Rebin(rebin[var_iter])
                        
                    if doNorm:
                        hist_nom = getNormhist(hist_nom)
                        h_new = getNormhist(h_new)
                        #h_new_d = getNormhist(h_new_d)
                        h_new.GetYaxis().SetTitle("Normalized Entries")
                    else:
                        h_new.GetYaxis().SetTitle("Entries")
                        
                    h_new.SetStats(0)
                    h_new.GetXaxis().SetLabelSize(0.);
                    h_new.GetXaxis().SetLabelSize(0.);
                    #h_new.SetTitle(region+keyName)
                    #h_new.SetTitle(keyName)
                    h_new.SetTitle(sample+"_"+region+"_"+variation)
                    h_new.GetXaxis().SetRangeUser(x_min[var_iter],x_max[var_iter])
                    #h_new.GetYaxis().SetRangeUser(y_min,y_max)
                    if(variables[var_iter]=="RNNScoreMerged"): 
                      variables[var_iter]="RNN score"
                    if(variables[var_iter]=="RNNScoreResolved"):
                      variables[var_iter]="RNN score"
                    if(variables[var_iter]=="MTagMerJets"):
                      variables[var_iter]="m^{tag}_{jj} [GeV]"
                    if(variables[var_iter]=="MTagResJets"):
                      variables[var_iter]="m^{tag}_{jj} [GeV]"
                    h_new.GetXaxis().SetTitle(variables[var_iter])
                    #marker style and color
                    hist_nom.SetLineColor(kRed)
                    hist_nom.SetMarkerColor(kRed)
                    hist_nom.SetMarkerStyle(20)
                    hist_nom.SetMarkerSize(1.1)
                    hist_nom.SetLineWidth(3)
                    h_new.SetLineWidth(3)
                    #h_new_d.SetLineWidth(3)
                    print("drawing histograms")
                    P_1.SetTicky(1)
                    h_new.Draw("E0 same")
                    hist_nom.Draw("E0 same")
                    #h_new_d.Draw("E0 same")

                    h_new.SetLineColor(kOrange+2)
                    #h_new_d.SetLineColor(kOrange+2)
                    #h_new.SetLineStyle(5)
                    #h_new_d.SetLineStyle(5)

                    leg=TLegend(0.57,0.77,0.85,0.90) #x1,y1,x2,y2

                    leg.SetBorderSize(0)
                    leg.SetFillStyle(0)
                    leg.SetTextSize(0.04)
                    leg.AddEntry(hist_nom,"Nominal","lep")
                    leg.AddEntry(h_new,"Down uncertainty","l")
                    #leg.AddEntry(h_new_d,"Down uncertainty","l")
                    leg.Draw()
                    
                    # Ratio 
                    P_2.cd()
                    h_ratio = getDataMCRatio(h_new,hist_nom)
                    #h_ratio_d = getDataMCRatio(h_new_d,hist_nom)
                    h_ratio.SetTitle("")
                    h_ratio.GetYaxis().SetTitle("#pm#sigma/Nominal")
                    h_ratio.GetYaxis().SetTitleSize(0.14);
                    h_ratio.GetYaxis().SetTitleOffset(0.3);
                    h_ratio.GetYaxis().SetLabelSize(0.1);
                    h_ratio.GetXaxis().SetTitleSize(0.14);
                    h_ratio.GetXaxis().SetTitleOffset(1.1);
                    h_ratio.GetXaxis().SetLabelSize(0.12);
                    h_ratio.SetLineWidth(3)
                    #h_ratio_d.SetLineColor(kOrange+2)
                    #h_ratio_d.SetLineWidth(3)
                    h_ratio.SetStats(0)
                    P_2.SetTicky(1)
                    h_ratio.Draw("hist")
                    #h_ratio_d.Draw("hist same")

                    h_ratio.GetYaxis().SetRangeUser(0.4,1.6)
                    #h_ratio.GetYaxis().SetRangeUser(0.0,2.0)
                    h_ratio.GetXaxis().SetTitle(variables[var_iter])

                    # Line
                    line = TLine(x_min[var_iter],1.0,x_max[var_iter],1.0)
                    line.SetLineStyle(2)
                    line.Draw()

                    canv.Draw()
                    if doNorm:
                        outputname = pdfdir+"/"+keyName+"_Norm.pdf"
                    else:
                        outputname =  pdfdir+"/"+keyName+".pdf"
                    canv.SaveAs(str(outputname))
            
             
                            
            region_iter+=1 #end of region loop
        var_iter+=1 #end of var loop
    sample_iter+=1

f.Close()

