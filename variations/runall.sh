#!/bin/sh

if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi
#setupATLAS

dir=/eos/atlas/unpledged/group-tokyo/users/mfujimot/VBS/CONDOR_output_forFitting

#1
inputSample1=$dir/Histograms.VBS.2lep_local.13TeV.mc16ade.08aug23_SigRename.root
#2
inputSample2=$dir/Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_EWQCD.root
outputSample2=$dir/Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_EWQCDSkim.root
#3
inputSample3=$dir/Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_QG.root
outputSample3=$dir/Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_QGSkim.root


#1. remove flavour unc. from original 
#python removeSyst.py --dir $inputSample1 --reg 0
#python removeSyst.py --dir $inputSample1  --reg 1

#2. remain only modeling 
#python removeSystOnly_v2.py --dir $inputSample2 --dirNew $outputSample --reg 0
#python removeSystOnly_v2.py --dir $inputSample2 --dirNew $outputSample --reg 1

#2. remain only EWQCDint (only in signal)
#python removeSystOnly_v2.py --dir $inputSample2 --dirNew $outputSample2 --reg 0
#python removeSystOnly_v2.py --dir $inputSample2 --dirNew $outputSample2 --reg 1

#3. get QG syst only
#python replace.py --dir $inputSample3 --dirNew $outputSample3 --reg 0
#python replace.py --dir $inputSample3 --dirNew $outputSample3 --reg 1


#########################################
#add all samples
# 1. original + 2. modelling + 2. EWQCD (Sig only) + 2.Shower (Sig only) + 3.QG + 4.Theory
#hadd $dir/Histograms.VBS.2lep_local.13TeV.mc16ade.08aug23_Syst_SigRename.root
#Histograms.VBS.2lep_local.13TeV.mc16ade.08aug23_SigRename.root
#Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_EWQCDSkim.root
#Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_Sh.root
#Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_QGSkim.root
#Histograms.VBS.2lep_local.13TeV.mc16ade.07aug23_QCDScale.root


