#to get only modeling NPs

import ROOT as r
import numpy as np
from array import array
#from optparse import OptionParser
import argparse
from ROOT import TH2F,TKDE, TCanvas,TFile, TCanvas, TStyle, TAxis, gStyle, TColor, TPad, TH1F, kBlue,kRed,kSpring,kBlack,kGray,kCyan,kOrange, kViolet, kGreen, gROOT, TLatex, TLine, TF1,TH1D, TGraphErrors, TAxis, kFullCircle, kFALSE,kTRUE, TLegend,kAzure, TGraph
from math import fabs
import glob
import math
import os

gROOT.SetBatch(kTRUE);

# Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--dir', help = 'path to root file with pdf uncertainties')
parser.add_argument('--dirNew', help = 'path to root file with pdf uncertainties')
parser.add_argument('--reg', type= int, help = '0 is resolved, 1 is boosted' )
args = parser.parse_args()


# Some functions
def getNormhist(hist):
    h_temp= hist.Clone()
    norm= 1/h_temp.Integral(0,h_temp.GetNbinsX()+1) #TH1::Integral returns the integral of bins in the bin range (default(1,Nbins), to include the Under/Overflow, use h.Integral(0,Nbins+1)
    h_temp.Scale(norm)

    return h_temp

def getDataMCRatio(h_data,h_MC):
    h_comp= h_data.Clone()
    h_comp.Reset()

    for bin_x in range(0,h_comp.GetNbinsX()+1):
        nData = h_data.GetBinContent(bin_x);
        eData = h_data.GetBinError(bin_x);
        nMC = h_MC.GetBinContent(bin_x);
        if nMC > 0:
            nComp = (nData) / nMC;
            eComp = eData / nMC;
            h_comp.SetBinContent(bin_x, nComp);
            h_comp.SetBinError(bin_x, eComp);
            pass

        pass
    pass
    return h_comp;


#uncertainty sets
sys_names = [
"EWQCDint",
#
"MODEL_Z_MGPy8",
#
]
#uncertainty sets for BTags
sys_names_btag = [
"FT_EFF_Eigen_B_0_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_B_1_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_B_2_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_0_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_1_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_2_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_C_3_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_0_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_1_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_2_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_3_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_Eigen_Light_4_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_extrapolation_AntiKt4EMPFlowJets_BTagging201903",
"FT_EFF_extrapolation_from_charm_AntiKt4EMPFlowJets_BTagging201903"
]


#--------------------------
# User configuration
#--------------------------------------------------------
filename= str(args.dir) 
f=r.TFile(filename,"UPDATE")
f_sys = f.GetDirectory("Systematics/");

#minori new sample
filenameNew= str(args.dirNew)#"/sps/atlas/m/mfujimot/mergedsamples/2202samplesfJvt/Histograms.VBS.2lep_local.13TeV.mc16ade.22fev22_ModelingRemained.root"
if int(args.reg) == 0:
	fNew=r.TFile(filenameNew,"RECREATE")
        fNew_sys = fNew.mkdir("Systematics/");
elif int(args.reg) == 1:
	fNew=r.TFile(filenameNew,"UPDATE")
        fNew_sys = fNew.GetDirectory("Systematics/");

# sample and associated pdf uncertainties array
samples = ["ZZ","WZ","WW","EW6llqq","Z","W","ttbar","stops","stopt","stopWtDilep","ZZllqqEW6", "WWlvqqEW6", "WZlvqqEW6", "WZllqqEW6"] 
uncertainty_sets = [sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names,sys_names] # each sample comes with its  corresponding uncertainty arrays_names_btag

if int(args.reg) == 0:
        #Resolved
        regions= ["SRVBS_Fid","SRVBS_Fid_HMlljj1500","SRVBS_Fid_LMlljj1500","CRVjet_Fid"]
        prefix = ["0ptag2pjet_0ptv","0ptag2pjet_0ptv","0ptag2pjet_0ptv","0ptag2pjet_0ptv"] #for each region you need to add the corresponding prefix 
        variables = ["RNNScoreResolved","MTagResJets"]#,"Mlljj"]#,
elif int(args.reg) == 1:
        # Merged 
        regions= ["SRVBS_HP","SRVBS_LP","SRVBS_HP_HMllJ1500","SRVBS_HP_LMllJ1500","SRVBS_LP_HMllJ1500","SRVBS_LP_LMllJ1500","CRVjet"] 
        #regions= ["SRVBS_HP","SRVBS_LP","CRVjet"]#,"SRVBS_HP_HMVV","SRVBS_HP_LMVV","SRVBS_LP_HMVV","SRVBS_LP_LMVV"] 
        prefix = ["0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv","0ptag1pfat0pjet_0ptv"]
        variables = ["RNNScoreMerged","MTagMerJets"]#,"MllJ"]#,
else:
        print("Invalid regime to scan")

doRebin = False
doNorm = False
doConPlots = True

#get names to delete
hnames_del = []
sample_iter=0
for sample in samples: #samples loop (Z)
    var_iter = 0
    for variable in variables: #var loop (RNNSCore)
        region_iter=0
        for region in regions: #region loop (SRVBS)

            # loop over variations
            for j in range(len(uncertainty_sets[sample_iter])): #syst loop
                uncertainty_set = uncertainty_sets[sample_iter]
                variation = variable+"_Sys"+str(uncertainty_set[j])
                keyName=sample+"_"+prefix[region_iter]+"_"+region+"_"+variation
                out_keyName=keyName+"__1up"
                hnames_del += [out_keyName]
                out_keyName_d=keyName+"__1down"
                hnames_del += [out_keyName_d]
                            
            region_iter+=1 #end of region loop
        var_iter+=1 #end of var loop
    sample_iter+=1

#minori 
####################

#get list of KEYS
hnames = []
for key in f_sys.GetListOfKeys():
    n = key.GetName()
    hnames += [n]

# for loop of histograms
for i in range(len(hnames)):
   for j in range(len(hnames_del)):
       if hnames[i] == hnames_del[j]:
          t = f_sys.Get(hnames[i])     # t is the histogram object: TH1 or TH2
          fNew_sys.WriteObject(t,hnames[i])

####################

f.Close()

