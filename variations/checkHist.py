import ROOT as r
import numpy as np
from array import array
#from optparse import OptionParser
import argparse
from ROOT import TH2F,TKDE, TCanvas,TFile, TCanvas, TStyle, TAxis, gStyle, TColor, TPad, TH1F, kBlue,kRed,kSpring,kBlack,kGray,kCyan,kOrange, kViolet, kGreen, gROOT, TLatex, TLine, TF1,TH1D, TGraphErrors, TAxis, kFullCircle, kFALSE,kTRUE, TLegend,kAzure, TGraph
from math import fabs
import glob
import math
import os

gROOT.SetBatch(kTRUE);

# Argument parser
parser = argparse.ArgumentParser()
#parser.add_argument('--dir', help = 'path to root file with pdf uncertainties')
#parser.add_argument('--reg', type= int, help = '0 is resolved, 1 is boosted' )
args = parser.parse_args()

# Some functions
def getNormhist(hist):
    h_temp= hist.Clone()
    norm= 1/h_temp.Integral(0,h_temp.GetNbinsX()+1) 
    #TH1::Integral returns the integral of bins in the bin range (default(1,Nbins), to include the Under/Overflow, use h.Integral(0,Nbins+1)
    h_temp.Scale(norm)

    return h_temp

def getDataMCRatio(h_data,h_MC):
    h_comp= h_data.Clone()
    h_comp.Reset()

    for bin_x in range(0,h_comp.GetNbinsX()+1):
        nData = h_data.GetBinContent(bin_x);
        eData = h_data.GetBinError(bin_x);
        nMC = h_MC.GetBinContent(bin_x);
        if nMC > 0:
            nComp = (nData) / nMC;
            eComp = eData / nMC;
            h_comp.SetBinContent(bin_x, nComp);
            h_comp.SetBinError(bin_x, eComp);
            #print(h_MC.GetBinCenter(bin_x),nComp)
            pass

        pass
    pass
    return h_comp;

def reweight(h_nom, h_rw):
    h_new = h_rw.Clone()
    h_new.Reset()
    
    ratio = getDataMCRatio(h_nom,h_rw)
    #print(ratio.GetBinContent(1), ratio.GetMean())
    ratio.Fit("pol0","Q")
    weight = ratio.GetFunction("pol0").GetParameter(0)
    #print("fit result weight:",weight)
    arr_val = []
    for bin_x in range(0,h_rw.GetNbinsX()+1):
        con = h_rw.GetBinContent(bin_x)*weight
        err = h_rw.GetBinError(bin_x)
        h_new.SetBinContent(bin_x,con)
        h_new.SetBinError(bin_x,err)
        
    return h_new

#--------------------------
# User configuration
#--------------------------------------------------------
filename="/eos/atlas/unpledged/group-tokyo/users/mfujimot/VBS/CONDOR_output_forFitting/Histograms.VBS.2lep_local.13TeV.mc16ade.04jan23_Syst.root"
f=r.TFile(filename,"READ")
f_sys = f.GetDirectory("Systematics/");

#get histogram
keyNameNom = "Z";

#get syst histogram
keyNameSyst = "Z_SysTheoryQCD_Z__1up";

doRebin = True
doNorm = True
doConPlots = True

f.cd()
hist_nom = f.Get(keyNameNom)

#mv into sys directory
f_sys.cd()
#get var hist 
hist_var = f_sys.Get(keyNameSyst)

if doRebin:
    hist_var.Rebin(10)

#######################
        
if doConPlots:
    print("printing plots")
    pdfdir = "./CheckPlots/"
    if not os.path.exists(pdfdir) :
        os.makedirs(pdfdir)

    canv=TCanvas("","",60,60,600,600)
    P_1 = TPad("Hists","", 0, 0.35, 0.94, 1);
    P_2 =TPad("Data/Bgd","", 0, 0.1, 0.94, 0.35); #xmin,ymin,xmax,ymax

    P_1.Draw();
    P_2.Draw();

    P_1.SetBottomMargin(0.02);
    P_1.SetTopMargin(0.07);
    P_1.SetRightMargin(0.15);
    P_1.SetLeftMargin(0.15);

    P_2.SetTopMargin(0.07);
    P_2.SetBottomMargin(0.3);
    P_2.SetRightMargin(0.15);
    P_2.SetLeftMargin(0.15);

    P_1.cd()

    if doNorm:
        hist_nom = getNormhist(hist_nom)
        hist_var = getNormhist(hist_var)
        hist_var.GetYaxis().SetTitle("Normalized Entries")
    else:
        hist_var.GetYaxis().SetTitle("Entries")

    hist_var.SetStats(0)
    hist_var.GetXaxis().SetLabelSize(0.);
    hist_var.GetXaxis().SetLabelSize(0.);
    hist_var.SetTitle(keyNameSyst)
    hist_var.GetXaxis().SetRangeUser(40,4000)
    hist_var.GetXaxis().SetTitle("MTagMerJets")
    #
    hist_nom.GetXaxis().SetRangeUser(40,4000)
    hist_nom.SetLineColor(kRed)
    hist_nom.SetMarkerColor(kRed)
    hist_nom.SetMarkerStyle(20)
    hist_nom.SetMarkerSize(1.1)
    hist_nom.SetLineWidth(3)
    hist_var.SetLineWidth(3)
    hist_var.Draw("E0 same")
    hist_nom.Draw("E0 same")
    hist_var.SetLineColor(kOrange+2)

    P_2.cd()
    h_ratio = getDataMCRatio(hist_var,hist_nom)
    h_ratio.SetTitle("")
    h_ratio.GetYaxis().SetTitle("#pm#sigma/Nominal")
    h_ratio.GetYaxis().SetTitleSize(0.14);
    h_ratio.GetYaxis().SetTitleOffset(0.3);
    h_ratio.GetYaxis().SetLabelSize(0.1);
    h_ratio.GetXaxis().SetTitleSize(0.14);
    h_ratio.GetXaxis().SetTitleOffset(1.1);
    h_ratio.GetXaxis().SetLabelSize(0.12);
    h_ratio.SetLineWidth(3)
    h_ratio.SetStats(0)
    h_ratio.Draw("hist")

    h_ratio.GetXaxis().SetRangeUser(40,4000)
    h_ratio.GetYaxis().SetRangeUser(0.8,1.2)
    h_ratio.GetXaxis().SetTitle("RNNScoreMerged")

    # Line
    line = TLine(40,1.0,4000,1.0)
    line.SetLineStyle(2)
    line.Draw()

    if doNorm:
        outputname = pdfdir+"/"+keyNameSyst+"_Norm.pdf"
    else:
        outputname =  pdfdir+"/"+keyNameSyst+".pdf"
    canv.SaveAs(str(outputname))

f.Close()

