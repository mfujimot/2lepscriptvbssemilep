import os
import re

root_directory = './WSMaker_outputfor2binRNNAll0501'
file_name = 'likelihood_scan_plots.log'
pattern = r'-' * 32 + r'\n(.*?)\n' + r'-' * 32 + r'\nhave the following 68% CI:\n(.*?mu_SemileptonicVBS_aQGC: \[.*?, .*?\])\n' + r'-' * 32 + r'\nhave the following 95% CI:\n(.*?mu_SemileptonicVBS_aQGC: \[.*?, .*?\])'

found_paths = []
for root, dirs, files in os.walk(root_directory):
    if file_name in files:
        found_paths.append(os.path.join(root, file_name))

if found_paths:
    for path in found_paths:
        with open(path, 'r') as file:
           path_parts = path.split('/')
           file_name = path_parts[-3]
           #print(file_name)
           af_match = re.search(r'_a(\w+)', file_name)
           c_match = re.search(r'_c(\w+)', file_name)
           if af_match and c_match:
              af_value = af_match.group(1)
              c_value = c_match.group(1)
              print(af_value.split('_')[0])
              print(c_value.split('_')[0])

           text = file.read()

        matches = re.findall(pattern, text, re.DOTALL)
        for match in matches:
           section_title = match[0].strip()
           ci_68 = match[1].strip()
           ci_95 = match[2].strip()
           #print(f"section: {section_title}")
           print(f"95% CI: {ci_95}")
           print()

else:
    print(f'{file_name} was not there')

