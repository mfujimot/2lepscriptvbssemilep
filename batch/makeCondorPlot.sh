#!/bin/sh

if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi
echo 'start script'
#setupATLAS

#inDir=/gpfs/fs2001/fujimoto/WSMaker_outputfor2binRNNAll1402/
inDir=/gpfs/fs2001/fujimoto/WSMaker_outputfor2binRNNAll2901/
#inDir=/gpfs/fs2001/fujimoto/WSMaker_outputfor2binRNNAll0501/
#inDir=/gpfs/fs2001/fujimoto/WSMaker_outputfor2binRNNAll0610/

#FILE_NAME=condorsubmitallLL${coefficient}.sub
FILE_NAME=condorsubmitallPlot.sub
BATCH_NAME=/usr/bin/bash
#JOB_NAME=bsubFitCondorPlot.sh
JOB_NAME=/home/fujimoto/WSMakeraQGC/batch/JobWrapperPlot.sh

echo 'executable =' ${JOB_NAME} >> ${FILE_NAME}
echo '' >>${FILE_NAME}

ls ${inDir} | while read dir
do
workSpace=${dir}
echo ${workSpace}
    # input
    #echo 'arguments = ' ${JOB_NAME} ${inDir} ${workSpace} >> ${FILE_NAME}
    echo 'arguments = '${inDir} ${workSpace} >> ${FILE_NAME}
    echo 'output = log/$(ClusterID).$(ProcID).out' >>${FILE_NAME}
    echo 'error = log/$(ClusterID).$(ProcID).err' >>${FILE_NAME}
    echo 'log = log/$(ClusterID).$(ProcID).log' >>${FILE_NAME}
    echo 'request_cpus   = 2' >>${FILE_NAME}
    echo 'request_memory = 4096' >>${FILE_NAME}
    echo '+job_queue = q2d' >>${FILE_NAME}
    echo '' >>${FILE_NAME}
    echo 'queue ' >>${FILE_NAME}
    echo '' >>${FILE_NAME}
done

echo 'done'

