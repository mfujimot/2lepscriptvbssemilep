# batch

## Getting started

First modify the directory paths in all of the scripts.

```
setupATLAS -c centos7 -b
```

To submit jobs for fittings.
```
source makeCondor.sh
condor_submit condorsubmitall.sub
```

Getting ll curves.
```
source makeCondorLL.sh
condor_submit condorsubmitallLL.sub
```

Getting pre/postfit plots.
```
source makeCondorPlot.sh
condor_submit condorsubmitallPlot.sub
```

Get all the CI limits.
```
python3 findLimit.py > limits.txt
```

Plot the limits in clipping points
Limit values are hardcoded
```
python3 plotLimits.py
```

Get the uniterized limits
Limit values are hardcoded
```
root -l -b -q aQGCfint.C
```
