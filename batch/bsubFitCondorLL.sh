#!/bin/sh

if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

echo "start script"
setupATLAS


inDir=$1
workSpace=$2
mass=1

tmpDir=/tmp/mfujimot
if [ ! -d ${tmpDir} ]; then
    mkdir ${tmpDir}
fi
cd ${tmpDir}
ls -ltrh

workDir=work_${workSpace}
if [ -d ${workDir} ]; then
    echo "work directory already exists: delete directory"
    rm -rf ${workDir}
fi

mkdir ${workDir}
cd ${workDir}

echo "now at work dir"

#cp all codes in need?
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/src .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/scripts .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/configs .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/WSMakerCore .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/util .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/include .
cp /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/CMakeLists.txt .
cp /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/setup.sh .
echo "now copied"

source /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/setup.sh

mkdir build
ls -ltrh

#compile
cd build
cmake ..
make -j5

cd ../
ls -ltrh

##run
#likelihood landscape observed
python WSMakerCore/scripts/runLikelihoodLandscape.py --infile_path ${inDir}/$workSpace/workspaces/combined/${mass}.root --outputdir ${inDir}/$workSpace/LikelihoodLandscape --ObsDataName obsData --algs Data --density 20 &> ${inDir}/$workSpace/logs/likelihoodLandscape_data.log
echo 'finished likelihood landscape'

rm ${inDir}/$workSpace/LikelihoodLandscape/Data/LikelihoodLandscape_out.root
hadd ${inDir}/$workSpace/LikelihoodLandscape/Data/LikelihoodLandscape_out.root ${inDir}/$workSpace/LikelihoodLandscape/Data/*.root
echo 'recreate output histogram'

#likelihood landscape expected
python WSMakerCore/scripts/runLikelihoodLandscape.py --infile_path ${inDir}/$workSpace/workspaces/combined/${mass}.root --outputdir ${inDir}/$workSpace/LikelihoodLandscape --ObsDataName asimovData --algs Asimov &> ${inDir}/$workSpace/logs/likelihoodLandscape_asimov.log
echo 'finished likelihood landscape'

rm ${inDir}/$workSpace/LikelihoodLandscape/Asimov/LikelihoodLandscape_out.root
hadd ${inDir}/$workSpace/LikelihoodLandscape/Asimov/LikelihoodLandscape_out.root ${inDir}/$workSpace/LikelihoodLandscape/Asimov/*.root
echo 'recreate output histogram'

#plot
python WSMakerCore/scripts/plotLikelihoodLandscape.py --outdir ${inDir}/$workSpace/LikelihoodLandscapePlots --asimov_infile ${inDir}/$workSpace/LikelihoodLandscape/Asimov/LikelihoodLandscape_out.root --data_infile ${inDir}/$workSpace/LikelihoodLandscape/Data/LikelihoodLandscape_out.root &> ${inDir}/$workSpace/logs/likelihood_scan_plots.log
echo 'finished ll plotting'

#ls
ls -ltrh

#rm workdir
cd ${tempDir}
rm -r ${workDir}

echo "finished"

