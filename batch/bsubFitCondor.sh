
echo "start script"

aQGCName=$1
clip=$2

tmpDir=/tmp/mfujimot
if [ ! -d ${tmpDir} ]; then
    mkdir ${tmpDir}
fi
cd ${tmpDir}
ls -ltrh

workDir=work_${aQGCName}_${clip}
if [ -d ${workDir} ]; then
    echo "work directory already exists: delete directory"
    rm -rf ${workDir}
fi

mkdir ${workDir}
cd ${workDir}

echo "now at work dir"

##cp all codes in need?
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/src .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/scripts .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/configs .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/WSMakerCore .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/util .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/include .
cp /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/CMakeLists.txt .
cp /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/setup.sh .
echo "now copied"

mkdir build
ls -ltrh

echo $ATLAS_LOCAL_ROOT_BASE
source /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/setup.sh

cd build

#compile
cmake ..
make -j5

cd ../
ls -ltrh

#run
python /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/scripts/Analysis_SemileptonicVBS.py SemileptonicVBS_mc16ade_0214_3Chan RNN_vvqq_lvqq_llqq_BoostedResolved_m1_p11_a${aQGCName}_c${clip}

ls -ltrh

#copy outputs
cp -r output/* /gpfs/fs2001/fujimoto/WSMaker_outputfor2binRNNAll1402/
echo "copied"
ls -ltrh

#rm workdir
cd ${tempDir}
rm -r ${workDir}

echo "finished"

