#! /bin/bash -l
#
#  This is a template for a batch job which you can use to submit 
#
# See also this link on how to submit to batch queues
# https://twiki.atlas-canada.ca/bin/view/AtlasCanada/ATLASLocalRootBase2#Batch_Jobs
#
#

inDir=$1
workSpace=$2

# Environment variables to pass on
export ALRB_testPath=",,,,"
export ALRB_CONT_SWTYPE="apptainer"
export ALRB_CONT_RUNPAYLOAD="source bsubFitCondorLL.sh $inDir $workSpace"
export ALRB_CONT_PRESETUP="hostname -f; date; id -a"
export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(serverurl=http://atlasfrontier1-ai.cern.ch:8000/atlr)(serverurl=http://atlasfrontier2-ai.cern.ch:8000/atlr)(proxyurl=http://frontier-squid.icepp.jp:3128)(proxyurl=http://frontier-squid1.icepp.jp:3128)(proxyurl=http://frontier-squid2.icepp.jp:3128)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)"

# ideally setupATLAS is defined by the site admins.  Just in case ....
alias | \grep -e "setupATLAS" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    typeset  -f setupATLAS > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	function setupATLAS
	{
            if [ -d  /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ]; then
		export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
		source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -m /home:/home -m /gpfs/fs2001:/gpfs/fs2001 $@
		return $?
            else
		\echo "Error: cvmfs atlas repo is unavailable"
		return 64
            fi
	}
    fi
fi

# setupATLAS -c <container> which will run and also return the exit code
#  (setupATLAS is source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh)
setupATLAS -c centos7
exit $?
