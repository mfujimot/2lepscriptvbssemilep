#!/bin/sh

if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi
echo 'start script'
#setupATLAS

FILE_NAME=condorsubmitall.sub
BATCH_NAME=/usr/bin/bash
#JOB_NAME=bsubFitCondor.sh
JOB_NAME=/home/fujimoto/WSMakeraQGC/batch/JobWrapper.sh

aQGCNames=(
    "FT0"
    "FT1"
    "FT2"
    "FT5"
    "FT6"
    "FT7"
    "FT8"
    "FT9"
    "FM0"
    "FM1"
    "FM2"
    "FM3"
    "FM4"
    "FM5"
    "FM7"
    "FS02"
    "FS1"
)

#    "FT0"
#    "FT1"
#    "FT2"
#    "FT5"
#    "FT6"
#    "FT7"
#    "FT8"
#    "FT9"
#    "FM0"
#    "FM1"
#    "FM2"
#    "FM3"
#    "FM4"
#    "FM5"
#    "FM7"
#    "FS02"
#    "FS1"

clips=(
    "1000"
)
#    "1500"
#    "2000"
#    "3000"
#    "5000"
#    ""

echo 'executable =' ${JOB_NAME} >> ${FILE_NAME}
echo '' >>${FILE_NAME}

for name in "${aQGCNames[@]}" ; do
  for clip in "${clips[@]}" ; do
    # input
    #echo 'arguments = '${JOB_NAME} ${name} ${clip} >> ${FILE_NAME}
    echo 'arguments = '${name} ${clip} >> ${FILE_NAME}
    echo 'output = log/$(ClusterID).$(ProcID).out' >>${FILE_NAME}
    echo 'error = log/$(ClusterID).$(ProcID).err' >>${FILE_NAME}
    echo 'log = log/$(ClusterID).$(ProcID).log' >>${FILE_NAME}
    echo 'request_cpus   = 2' >>${FILE_NAME}
    echo 'request_memory = 4096' >>${FILE_NAME}
    echo '+job_queue = q2d' >>${FILE_NAME}
    echo '' >>${FILE_NAME}
    echo 'queue ' >>${FILE_NAME}
    echo '' >>${FILE_NAME}
  done
done

echo 'done'

