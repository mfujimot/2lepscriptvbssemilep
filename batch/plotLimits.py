import ROOT as r
import numpy as np
from array import array
#from optparse import OptionParser
import argparse

from ROOT import TH2F,TKDE, TCanvas,TFile, TCanvas, TStyle, TAxis, gStyle, TColor, TPad, TH1F, kBlue,kRed,kSpring,kBlack,kGray,kCyan,kOrange, kViolet, kGreen, gROOT, TLatex, TLine, TF1,TH1D, TGraphErrors, TAxis, kFullCircle, kFALSE,kTRUE, TLegend,kAzure, TGraph, TSpline3
from math import fabs
import glob
import math
import os

gROOT.SetBatch(kTRUE);

gROOT.SetMacroPath(os.pathsep.join([gROOT.GetMacroPath(), "/afs/cern.ch/user/m/mfujimot/public/atlasstyle-00-04-02/"]))
gROOT.LoadMacro("AtlasLabels.C")
gROOT.LoadMacro("AtlasStyle.C")
gROOT.LoadMacro("AtlasUtils.C")
from ROOT import ATLASLabel, SetAtlasStyle
#SetAtlasStyle()

aQGCnames = ["FT0","FT1","FT2","FT5","FT6","FT7","FT8","FT9","FM0","FM1","FM2","FM3","FM4","FM5","FM7"]
#aQGCnames = ["FS02","FS1"]

for aQGCname in aQGCnames:
   #graph
   x = array('d', [1.5, 2.0, 3.0, 5.0])
   yf = TF1;
   yfm = TF1;
   # [ 1500, 2000, 3000, non-clip]
   if aQGCname == "FT0":  #ok
      y_obs_u = array('d', [  0.81,  0.47,  0.28,  0.22])
      y_obs_d = array('d', [ -1.07, -0.59, -0.33, -0.25])
      y_exp_u = array('d', [  0.95,  0.43,  0.23,  0.18])
      y_exp_d = array('d', [ -1.09, -0.51, -0.27, -0.20])
      yf = TF1("f1","12.*pi/5./pow(x,4)",0,10)
      yfm = TF1("f1","-12.*pi/5./pow(x,4)",0,10)
   elif aQGCname == "FT1": #ok
      y_obs_u = array('d', [  0.98,  0.54,  0.30,  0.24]) 
      y_obs_d = array('d', [ -0.98, -0.54, -0.30, -0.24]) 
      y_exp_u = array('d', [  1.10,  0.46,  0.25,  0.19])
      y_exp_d = array('d', [ -1.09, -0.46, -0.25, -0.19])
      yf = TF1("f1","24.*pi/5./pow(x,4)",0,10)
      yfm = TF1("f1","-24.*pi/5./pow(x,4)",0,10)
   elif aQGCname == "FT2": #ok
      y_obs_u = array('d', [  1.95,  1.23,  0.69,  0.56]) 
      y_obs_d = array('d', [ -1.94, -1.23, -0.69, -0.56]) 
      y_exp_u = array('d', [  2.44,  1.07,  0.58,  0.45])
      y_exp_d = array('d', [ -2.43, -1.06, -0.57, -0.44])
      yf = TF1("f1","96.*pi/13./pow(x,4)",0,8)
      yfm = TF1("f1","-96.*pi/13./pow(x,4)",0,8)
   elif aQGCname == "FT5": #ok
      y_obs_u = array('d', [  2.70,  1.24,  0.72,  0.59]) 
      y_obs_d = array('d', [ -2.75, -1.37, -0.80, -0.64]) 
      y_exp_u = array('d', [  2.65,  1.17,  0.66,  0.53])
      y_exp_d = array('d', [ -2.97, -1.31, -0.73, -0.57])
      yf = TF1("f1","8.*pi/sqrt(3)/pow(x,4)",0,8)
      yfm = TF1("f1","-8.*pi/sqrt(3)/pow(x,4)",0,8)
   elif aQGCname == "FT6": #ok
      y_obs_u = array('d', [  3.43,  1.46,  0.86,  0.72]) 
      y_obs_d = array('d', [ -3.58, -1.55, -0.90, -0.74])
      y_exp_u = array('d', [  3.19,  1.49,  0.86,  0.72]) 
      y_exp_d = array('d', [ -3.42, -1.61, -0.92, -0.76])
      yf = TF1("f1","48.*pi/7./pow(x,4)",0,8)
      yfm = TF1("f1","-48.*pi/7./pow(x,4)",0,8)
   elif aQGCname == "FT7": #ok
      y_obs_u = array('d', [  7.96,  3.34,  2.03,  1.72]) 
      y_obs_d = array('d', [ -8.88, -3.96, -2.39, -1.96])
      y_exp_u = array('d', [  8.05,  3.34,  1.92,  1.57])
      y_exp_d = array('d', [ -9.55, -3.93, -2.23, -1.78])
      yf = TF1("f1","32.*pi/sqrt(3)/pow(x,4)",0,8)
      yfm = TF1("f1","-32.*pi/sqrt(3)/pow(x,4)",0,8)
   elif aQGCname == "FT8": #ok
      y_obs_u = array('d', [  3.28,  1.17,  0.62,  0.48]) 
      y_obs_d = array('d', [ -3.28, -1.17, -0.62, -0.48]) 
      y_exp_u = array('d', [  2.72,  1.30,  0.74,  0.59]) 
      y_exp_d = array('d', [ -2.71, -1.30, -0.74, -0.59]) 
      yf = TF1("f1","3.*pi/2./pow(x,4)",0,8)
      yfm = TF1("f1","-3.*pi/2./pow(x,4)",0,8)
   elif aQGCname == "FT9": #ok
      y_obs_u = array('d', [  7.77,  2.48,  1.35,  1.04]) 
      y_obs_d = array('d', [ -7.75, -2.47, -1.35, -1.03])
      y_exp_u = array('d', [  5.97,  2.70,  1.54,  1.22])
      y_exp_d = array('d', [ -5.96, -2.69, -1.54, -1.22])
      yf = TF1("f1","24.*pi/7./pow(x,4)",0,8)
      yfm = TF1("f1","-24.*pi/7./pow(x,4)",0,8)
   elif aQGCname == "FM0": #ok
      y_obs_u = array('d', [  4.95,  2.56,  1.58,  1.27])  
      y_obs_d = array('d', [ -5.03, -2.63, -1.60, -1.27])
      y_exp_u = array('d', [  5.98,  2.59,  1.44,  1.13])
      y_exp_d = array('d', [ -5.99, -2.60, -1.45, -1.13])
      yf = TF1("f1","32.*pi/sqrt(6)/pow(x,4)",0,8)
      yfm = TF1("f1","-32.*pi/sqrt(6)/pow(x,4)",0,8)
   elif aQGCname == "FM1": #ok
      y_obs_u = array('d', [  13.47,  8.05,  4.76,  3.98]) 
      y_obs_d = array('d', [ -13.48, -8.03, -4.75, -3.97])
      y_exp_u = array('d', [  16.75,  7.03,  4.02,  3.24]) 
      y_exp_d = array('d', [ -16.72, -7.03, -4.00, -3.24])
      yf = TF1("f1","128.*pi/sqrt(6)/pow(x,4)",0,8)
      yfm = TF1("f1","-128.*pi/sqrt(6)/pow(x,4)",0,8)
   elif aQGCname == "FM2": #ok
      y_obs_u = array('d', [  7.89,  3.95,  2.26,  1.86]) 
      y_obs_d = array('d', [ -7.89, -3.96, -2.26, -1.86])
      y_exp_u = array('d', [  8.48,  3.78,  2.10,  1.67])
      y_exp_d = array('d', [ -8.51, -3.79, -2.10, -1.66])
      yf = TF1("f1","16.*pi/sqrt(2)/pow(x,4)",0,8)
      yfm = TF1("f1","-16.*pi/sqrt(2)/pow(x,4)",0,8)
   elif aQGCname == "FM3": #
      y_obs_u = array('d', [  23.91,  11.17,  6.92,  5.75]) 
      y_obs_d = array('d', [ -23.02, -11.12, -6.92, -5.74])
      y_exp_u = array('d', [  25.45,  11.16,  6.49,  5.29]) 
      y_exp_d = array('d', [ -25.59, -11.23, -6.51, -5.29])
      yf = TF1("f1","64.*pi/sqrt(2)/pow(x,4)",0,8)
      yfm = TF1("f1","-64.*pi/sqrt(2)/pow(x,4)",0,8)
   elif aQGCname == "FM4": #ok
      y_obs_u = array('d', [  11.52,  5.61,  3.47,  2.99]) 
      y_obs_d = array('d', [ -11.45, -5.57, -3.45, -2.99])
      y_exp_u = array('d', [  11.95,  5.42,  3.16,  2.62]) 
      y_exp_d = array('d', [ -12.09, -5.44, -3.17, -2.62])
      yf = TF1("f1","32.*pi/pow(x,4)",0,8)
      yfm = TF1("f1","-32.*pi/pow(x,4)",0,8)
      yf = TF1("f1","32.*pi/sqrt(6)/pow(x,4)",0,8)
      yfm = TF1("f1","-32.*pi/sqrt(6)/pow(x,4)",0,8)
   elif aQGCname == "FM5": #ok
      y_obs_u = array('d', [  16.09,  8.33,  5.26,  4.48]) 
      y_obs_d = array('d', [ -16.06, -8.29, -5.24, -4.45])
      y_exp_u = array('d', [  16.75,  7.89,  4.64,  3.84]) 
      y_exp_d = array('d', [ -16.74, -7.90, -4.62, -3.81])
      yf = TF1("f1","64.*pi/pow(x,4)",0,8)
      yfm = TF1("f1","-64.*pi/pow(x,4)",0,8)
   elif aQGCname == "FM7": #ok 
      y_exp_u = array('d', [  22.62,  12.98,  7.65,  6.48])
      y_exp_d = array('d', [ -25.95, -13.70, -7.99, -6.65])
      y_obs_u = array('d', [  26.62,  11.32,  6.43,  5.21]) 
      y_obs_d = array('d', [ -27.55, -11.63, -6.59, -5.33])
      yf = TF1("f1","256.*pi/sqrt(6)/pow(x,4)",0,8)
      yfm = TF1("f1","-256.*pi/sqrt(6)/pow(x,4)",0,8)
   elif aQGCname == "FS02": #ok
      y_obs_u = array('d', [   8.65,  6.23,  4.30,  3.99]) 
      y_obs_d = array('d', [  -8.63, -6.20, -4.29, -3.99])
      y_exp_u = array('d', [  11.67,  5.69,  3.62,  3.23]) 
      y_exp_d = array('d', [ -11.64, -5.66, -3.61, -3.22])
      yf = TF1("f1","32.*pi/pow(x,4)",0,8)
      yfm = TF1("f1","-32.*pi/pow(x,4)",0,8)
   elif aQGCname == "FS1": #
      y_obs_u = array('d', [  21.36,  12.83,  8.91,  8.12]) 
      y_obs_d = array('d', [ -21.14, -12.82, -8.90, -8.11]) 
      y_exp_u = array('d', [  24.67,  11.84,  7.74,  6.87]) 
      y_exp_d = array('d', [ -24.63, -11.84, -7.72, -6.86]) 
      yf = TF1("f1","96.*pi/7./pow(x,4)",0,8)
      yfm = TF1("f1","-96.*pi/7./pow(x,4)",0,8)
   ymax  =  max(y_obs_u)
   #tgraph
   g_exp_u = TGraph(4,x,y_exp_u);
   s = TSpline3("grs",g_exp_u);
   g_exp_d = TGraph(4,x,y_exp_d);
   g_obs_u = TGraph(4,x,y_obs_u);
   g_obs_d = TGraph(4,x,y_obs_d);
   #set graph 
   g_exp_u.GetXaxis().SetRangeUser(0.,4.);
   g_exp_u.SetMinimum(-ymax-0.5);
   g_exp_u.SetMaximum(ymax+0.5);
   g_exp_u.SetTitle(";m_{VV} clipping value [TeV]; f / #Lambda^{4} [1/TeV^{4}]");
   g_exp_u.GetXaxis().SetTitleSize(0.05);
   #inft label
   g_exp_u.GetXaxis().SetLabelOffset(100);
   label = TLatex();
   label.SetTextSize(0.04);
   label.SetTextFont(42)
   ##
   g_exp_u.GetYaxis().SetTitleSize(0.05);
   g_exp_u.GetYaxis().SetLabelSize(0.04);
   g_exp_u.GetYaxis().SetTitleOffset(0.8);
   g_exp_u.SetLineColor(kGreen+2);
   g_exp_u.SetLineWidth(4);
   g_exp_u.SetLineStyle(2);
   #g_exp_u.SetFillColor(kGreen+2);
   #g_exp_u.SetFillStyle(3005);
   #
   g_exp_d.SetLineColor(kGreen+2);
   g_exp_d.SetLineWidth(4);
   g_exp_d.SetLineStyle(2);
   #g_exp_d.SetFillColor(kGreen+2);
   #g_exp_d.SetFillStyle(3005);
   #
   g_obs_u.SetLineColor(kBlue);
   g_obs_u.SetLineWidth(4);
   #g_obs_u.SetFillColor(kBlue);
   #g_obs_u.SetFillStyle(3004);
   #
   g_obs_d.SetLineColor(kBlue);
   g_obs_d.SetLineWidth(4);
   #g_obs_d.SetFillColor(kBlue);
   #g_obs_d.SetFillStyle(3004);
   #
   yf.SetLineColor(kBlack);
   yf.SetLineWidth(3);
   #yf.SetFillColor(kBlack);
   #yf.SetFillStyle(3003);
   #
   yfm.SetLineColor(kBlack);
   yfm.SetLineWidth(3);
   #yfm.SetFillColor(kBlack);
   #yfm.SetFillStyle(3003);
   #legend
   leg=TLegend(0.46,0.75,0.90,0.88) 
   leg.SetBorderSize(0)
   leg.SetFillStyle(0)
   leg.SetTextSize(0.04)
   leg.AddEntry(yf, "Theoretical unitarity bound")
   leg.AddEntry(g_exp_u, "Expected 95% CI limit")
   leg.AddEntry(g_obs_u, "Observed 95% CI limit")
   l=TLatex()
   l.SetTextSize(0.040)
   l.SetNDC()
   l.SetTextColor(kBlack)
   l.SetTextFont(52)
   lname=TLatex()
   lname.SetTextSize(0.040)
   lname.SetNDC()
   lname.SetTextColor(kBlack)
   lname.SetTextFont(62)
   #line
   line = TLine(1.2,0.,4.,0.)
   line.SetLineColor(kBlack)
   line.SetLineStyle(3)
   #draw
   canv = TCanvas("","",60,60,800,600)
   g_exp_u.Draw("AL");
   g_exp_d.Draw("L");
   g_obs_u.Draw("L");
   g_obs_d.Draw("L");
   yf.Draw("same")
   yfm.Draw("same")
   leg.Draw()
   line.Draw()
   # s.Draw("same");
   lname.DrawLatex(0.48,0.70,aQGCname)
   l.DrawLatex(0.55,0.70,"#it{#sqrt{s} = 13 TeV, 139 fb^{-1}}")
   if aQGCname == "FS02":
     offset = 4.2
   elif aQGCname == "FS1":
     offset = 5.2
   elif aQGCname == "FT0":
     offset = 0.65
   elif aQGCname == "FT1":
     offset = 0.65
   elif aQGCname == "FT2":
     offset = 0.8
   elif aQGCname == "FT5":
     offset = 0.8
   elif aQGCname == "FT6":
     offset = 0.9
   elif aQGCname == "FT7":
     offset = 1.3
   elif aQGCname == "FT8":
     offset = 0.9
   elif aQGCname == "FT9":
     offset = 1.2
   elif aQGCname == "FM0":
     offset = 1.2
   elif aQGCname == "FM1":
     offset = 1.8
   elif aQGCname == "FM2":
     offset = 1.3
   elif aQGCname == "FM3":
     offset = 2.6
   elif aQGCname == "FM4":
     offset = 1.6
   elif aQGCname == "FM5":
     offset = 2.5
   elif aQGCname == "FM7":
     offset = 2.8
   label.DrawLatex(1.40,-ymax-offset,"1.5");
   label.DrawLatex(1.96,-ymax-offset,"2");
   label.DrawLatex(2.96,-ymax-offset,"3");
   label.DrawLatex(7.96,-ymax-offset,"#infty");
   #label
   ATLASLabel(0.67,0.16,"Internal")
   #tick
   canv.SetTicks()
   #
   outputname =  aQGCname+"limit.pdf"
   canv.SaveAs(str(outputname))

