#!/bin/sh

if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

echo "start script"
setupATLAS


inDir=$1
workSpace=$2
mass=1

tmpDir=/tmp/mfujimot
if [ ! -d ${tmpDir} ]; then
    mkdir ${tmpDir}
fi
cd ${tmpDir}
ls -ltrh

workDir=work_${workSpace}
if [ -d ${workDir} ]; then
    echo "work directory already exists: delete directory"
    rm -rf ${workDir}
fi

mkdir ${workDir}
cd ${workDir}

echo "now at work dir"

#cp all codes in need?
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/src .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/scripts .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/configs .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/WSMakerCore .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/util .
cp -r /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/include .
cp /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/CMakeLists.txt .
cp /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/setup.sh .
echo "now copied"

source /home/fujimoto/WSMakeraQGC/WSMaker_VBSVV/setup.sh

mkdir build
ls -ltrh

#compile
cd build
cmake ..
make -j5

cd ../
ls -ltrh

##run
#plot fromWS 
python WSMakerCore/scripts/doPlotFromWS.py -m ${mass} -p 2 ${inDir}/$workSpace > ${inDir}/$workSpace/logs/doPlotFromWS.log 
#make table from plot
python WSMakerCore/scripts/makeTables.py -m ${mass} -t 2 ${inDir}/$workSpace > ${inDir}/$workSpace/logs/table.log 2>&1

# prefit
#plot fromWS 
python WSMakerCore/scripts/doPlotFromWS.py -m ${mass} -p 0 ${inDir}/$workSpace > ${inDir}/$workSpace/logs/doPlotFromWSPre.log 
#make table from plot
python WSMakerCore/scripts/makeTables.py -m ${mass} -t 0 ${inDir}/$workSpace > ${inDir}/$workSpace/logs/tablePre.log 2>&1

echo 'finished plotting and table'

#ls
ls -ltrh

#rm workdir
cd ${tempDir}
rm -r ${workDir}

echo "finished"

